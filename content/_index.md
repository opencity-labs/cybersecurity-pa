## Introduzione

{{% paragraph mw-40 %}}
Partendo da diversi documenti pubblicati in questi anni sul tema CyberSecurity nella PA, ovvero:

* [Misure Minime di Sicurezza ICT](https://www.agid.gov.it/it/sicurezza/misure-minime-sicurezza-ict)
* [Piano triennale per la PA](https://pianotriennale-ict.italia.it/)

e considerando le SANS 20

* [Sans 20](https://www.sans.org/course/implementing-auditing-critical-security-controls)

e con l'entrata in vigore del

* [GDPR](https://www.garanteprivacy.it/regolamentoue)

che ha migliorato nettamente la comprensione della privacy all’interno dei piccoli comuni (laddove implementato perlomeno un registro dei trattamenti e laddove fatta un’analisi del rischio) si è cercato di scrivere una guida in 10 punti, quindi breve ma chiara sia per addetti ai lavori che non, per migliorare la sicurezza informatica negli enti di piccole dimensioni (PAL).

Questo allo scopo di unire i principi di privacy e security, due facce della stessa medaglia, e dare delle linee pratiche di azione.
{{% /paragraph %}}



## CyberPareto

{{% paragraph mw-40 %}}
Il [principio di Pareto](https://it.wikipedia.org/wiki/Principio_di_Pareto) dice che 
“la maggior parte degli effetti è dovuta a un numero ristretto di cause”. 

L’obiettivo del documento è quindi di individuare il minor numero possibile di cause che generano il maggior numero di problemi di CyberSecurity nelle PAL.
{{% /paragraph %}}



## <a name="umani"></a>Standard di Sicurezza Minima: Umani

{{% paragraph mw-40 %}}
Un essere umano è il pericolo numero uno per chi fa CyberSicurezza.
{{% /paragraph %}}

_Legenda:_

* **BR** : basso rischio
* **MR**: medio rischio
* **AR**: alto rischio
* **TR**: task ricorrente

{{< table "table table-hover" >}}
| Standards                    | TR | Cosa fare                                         | BR | MR | AR |
| -----------------------------|----|---------------------------------------------------|----|----|----|
| Formazione Sicurezza         |![task ricorrente](./images/icon-recurring.png)|Formare ogni anno per 2 ore il personale sui rischi della cybersicurezza, <br> derivante dalla poca consapevolezza del funzionamento degli attacchi<br> informatici. <br><br>In particolare formare sui 3 vettori di attacco principali indicati dal Rapporto Clusit dell’anno precedente.<br><br> Alla data di scrittura:<br> * phishing<br>* spear phishing<br>* social engineering<br><br>Sottolineare l’importanza della gestione password con un password manager. |![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Formazione Privacy           |![task ricorrente](./images/icon-recurring.png)|Formare ogni anno per 2 ore il personale sui rischi derivanti dalla poca <br> conoscenza del valore dei dati. |![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
{{</ table >}}



## <a name="endpoint"></a>Standard di sicurezza minima: EndPoint

{{% paragraph mw-40 %}}
Un endpoint è un personal computer desktop, un notebook o un dispositivo mobile.
{{% /paragraph %}}

_Legenda:_

* **BR** : basso rischio
* **MR**: medio rischio
* **AR**: alto rischio
* **TR**: task ricorrente

{{< table "table table-hover" >}}
| Standards                    | TR | Cosa fare                                         | BR | MR | AR |
| -----------------------------|----|---------------------------------------------------|----|----|----|
| Patching Sistema Operativo   |![task ricorrente](./images/icon-recurring.png)|Applicare le patch di sistema operativo almeno 1 volta al mese, sia per <br> Windows (meglio dopo il [Patch Tuesday](https://it.wikipedia.org/wiki/Patch_Tuesday) che per altri sistemi operativi. <br> Utilizzare sistemi operativi manutenuti.<br> |![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Patching Software            |![task ricorrente](./images/icon-recurring.png)|Applicare le patch degli applicativi verificandone la presenza almeno 1 volta <br> al mese. Utilizzare applicazioni manutenute.<br> |![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Cifratura dati               | |Abilitare FileVault2 for Mac, Bitlocker for Windows sui notebook, e cifrare <br> smartphone e usb disk e key.<br> |![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Protezione dai malware       |![task ricorrente](./images/icon-recurring.png)|Installare l’antivirus su ogni endpoint (inclusi i device mobili). Verificare che <br>l’antivirus permetta sia la ricerca di malware mediante firme che euristiche <br>o intelligenza artificiale. L’aggiornamento delle firme antivirus deve avvenire <br>almeno giornalmente. Impostare una scansione completa dei client almeno settimanale. <br> |![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Copia dati                   |![task ricorrente](./images/icon-recurring.png)|Nessun dato deve rimanere sul client se non per il tempo necessario alla sua <br> elaborazione. Salvare i dati su server o in cloud in modo che il backup venga <br> garantito dal backup del server o del fornitore cloud, in maniera centralizzata. <br> |![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Permessi utente              | |Dare agli utenti i minimi permessi necessari all’utilizzo della postazione. <br> Togliere il permesso “administrator” locale della macchina”. <br> |![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Dispositivi Removibili       | |Impostare la scansione di dispositivi removibili. Cifrare i dispositivi removibili. <br> | |![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
{{</ table >}}



## <a name="server"></a>Standard di sicurezza minima: Server

{{% paragraph mw-40 %}}
Un server è un computer ad alta potenza e capacità che fornisce almeno un servizio di rete.
{{% /paragraph %}}

_Legenda:_

* **BR** : basso rischio
* **MR**: medio rischio
* **AR**: alto rischio
* **TR**: task ricorrente

{{< table "table table-hover" >}}
| Standards                    | TR | Cosa fare                                         | BR | MR | AR |
| -----------------------------|----|---------------------------------------------------|----|----|----|
| Patching Sistema Operativo   |![task ricorrente](./images/icon-recurring.png)|Applicare le patch di sistema operativo almeno 1 volta al mese, sia per <br> Windows (meglio dopo il [Patch Tuesday](https://it.wikipedia.org/wiki/Patch_Tuesday) che per altri sistemi operativi. <br> Utilizzare sistemi operativi manutenuti.<br> |![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Patching Software            |![task ricorrente](./images/icon-recurring.png)|Applicare le patch degli applicativi verificandone la presenza almeno 1 volta al <br> mese, inclusi eventuali hypervisor (sistemi di virtualizzazione). <br>Utilizzare software manutenuti.<br>|![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Vulnerability Management     |![task ricorrente](./images/icon-recurring.png)|Effettuare mensilmente una scansione delle vulnerability con strumento proprietario<br> o con Greenbone (o equivalente). Risolvere le vulnerabilità di livello critico e alto <br> in meno di 60 giorni dall’individuazione.<br>|![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Firewall                     | |Abilitare il firewall di sistema operativo in modo che blocchi qualsiasi connessione, tranne quelle autorizzate.<br>|![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Controllo Accessi            |![task ricorrente](./images/icon-recurring.png)|Verificare almeno semestralmente gli accessi al server, disabilitare gli utenti non <br> più utilizzati, applicare una password policy per gli utenti che hanno <br> accesso al server con almeno 14 caratteri alfanumerici, con almeno una <br> maiuscola e un carattere speciale. <br> |![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Copia dati                   |![task ricorrente](./images/icon-recurring.png)|Effettuare una copia dei dati sia locale (cifrata) che remota (offline, offsite, <br>cifrata sia in transito che sul dispositivo remoto) giornaliera. <br>Se possibile (ambiente virtuale) fare una copia completa delle macchine virtuali per un migliore ripristino del sistema. <br><br>Effettuare un test di restore delle informazioni a campione o di una <br>virtual machine almeno ogni 6 mesi. <br>|![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Protezione dai malware       |![task ricorrente](./images/icon-recurring.png)|Installare l’antivirus su ogni server. Verificare che l’antivirus permetta sia la <br>ricerca di malware mediante firme che euristiche o intelligenza artificiale. <br><br> L’aggiornamento delle firme antivirus deve avvenire almeno giornalmente. <br>Impostare una scansione completa del server almeno settimanale. <br> | |![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Protezione fisica            | |Mettere il server o i server in un locale chiuso a chiave, climatizzato e protetto da ups. <br> | |![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Log Centralizzati            | |Salvare i log del sistema operativo e delle applicazioni in un server centralizzato o in cloud. <br> | |![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Privacy Compliance           | |Verificare di avere un registro di trattamento dei dati presenti sui server / servers. <br> | |![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Monitoring                   | |Prevede un minimo di sistema di monitoraggio dei server (consigliato stato hardware <br>incluso di dischi, spazio disco, ram, cpu) e tenere il sistema in garanzia. <br> | |![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
{{</ table >}}




## <a name="network"></a>Standard di sicurezza minima: Network

{{% paragraph mw-40 %}}
La rete è il punto di raccordo di tutti i dispositivi informatici e il punto di raccordo ad internet.
{{% /paragraph %}}

_Legenda:_

* **BR** : basso rischio
* **MR**: medio rischio
* **AR**: alto rischio
* **TR**: task ricorrente

{{< table "table table-hover" >}}
| Standards                    | TR | Cosa fare                                         | BR | MR | AR |
| -----------------------------|----|---------------------------------------------------|----|----|----|
| Firewall                     |![task ricorrente](./images/icon-recurring.png)|Prevedere un firewall di Nuova Generazione (che sia in grado di analizzare i livelli ISO-OSI da layer 3 a 7). <br><br>Filtrare il traffico sia in entrata che in uscita dalle reti dell’ente verso internet. <br><br>Attivare antivirus, ids e ips perlomeno sul traffico generico. <br><br>Attivare deep inspection ssl. <br><br>Aggiornare il firewall almeno semestralmente a livello di sistema operativo. Avere un firewall in manutenzione. <br> <br> Utilizzare sistemi operativi manutenuti.<br> |![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Switching                    |![task ricorrente](./images/icon-recurring.png)|Avere uno switch in manutenzione. Aggiornare il sistema operativo almeno 1 volta l’anno. <br><br> |![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Zoning                       | |Separare il traffico delle reti in base alla tipologia di utenza (es. lan del <br>comune, videosorveglianza, biblioteca, wifi) <br> |![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Wifi                         | |Prevedere una rete guest, per i cittadini (ed eventuali dispositivi personali di <br>dipendenti e amministratori, sia mobili che fissi), e una <br>rete LAN per dipendenti e amministratori del comune. <br>Prevedere qos in modo che venga data priorità all’uso degli operatori <br>dell’ente e limitare la banda in uso al free wifi. <br> |![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Copia dati                   |![task ricorrente](./images/icon-recurring.png)|Salvare le configurazioni dei dispositivi di rete su server in una cartella riservata. <br> |![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
| Cifratura                    | |Permettere la gestione  degli apparati di rete solo con cifratura (https/ssh). <br> |![basso rischio](./images/icon-green.png)|![medio rischio](./images/icon-orange.png)|![alto rischio](./images/icon-red.png)|
{{</ table >}}

## <a name="cosamanca"></a>Cosa Potrebbe Mancare

* **inventario hw e sw**: il rischio è che diventi obsoleto velocemente (per i pc si suggerisce l’uso di GLPI glpi-project.org, che produce un inventario - volendo - sempre aggiornato al logon di dominio)
 
* **standard configurazioni**: tema troppo articolato per i piccoli comuni, da riversare sui fornitori
 
* **incident response**: un minimo e' effettuato mediante il GDPR con la policy di DATA BREACH
 
* **monitoring**: un mondo che la PA non conosce o ignora (strumenti free sono: zabbix o similari, ce ne sono a iosa!)

* **piano disaster recovery**: sarebbe l’obiettivo finale, ma ci vuole “incrementalità e iteratività per arrivarci

